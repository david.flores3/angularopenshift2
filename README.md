# vainilla-nodejs
***
Plantilla incial para para proyectos Node

## 🚀 Comenzando 
***
Descarga desde el sitio ofical [Nodejs](http://nodejs.org) utilizar la version lts.

## 🛅 Tecnología 
***
[JavaScritp]-Lenguaje de progamacion que utiliza el standar ECMAScript

[TypeScript]-Lenguaje derivado de javascript

[GitLab]-Repositorio Git

[SonarQube]-Calidad y seguridad de código

[OpenShift]-Plataforma para desarrollar contenedores

### 📋 Pre-requisitos
***
1. Visual studio code - editor

#### Plugins
1. npm - extencion de visual estudio para ejecutar el servidor de nodejs
2. EsLint - validardor de sintaxis.

## 🔧 Instalación 
***
Hacer doble click en el instalador de nodejs y seguir las intruciones del asistente.
 
## 📖 WIKI 
***

 
## PREGUNTAS
***
 
### Área para colocar los links y referencias a ser utilizadas
 
[GitLab]: <http://ci-hiperion.sefin.gob.hn/>
[node.js]: http://nodejs.org
[GitLab]: <http://ci-hiperion.sefin.gob.hn/>
[SonarQube]: <http://ci-hiperion.sefin.gob.hn/>
[OpenShift]: <http://ci-hiperion.sefin.gob.hn/>
[Nexus ]: <http://ci-hiperion.sefin.gob.hn/>
[Apicuro]: <http://ci-hiperion.sefin.gob.hn/>
[PAM]: <http://ci-hiperion.sefin.gob.hn/>
[Kaili]: <http://ci-hiperion.sefin.gob.hn/>
[3Scale]: <http://ci-hiperion.sefin.gob.hn/>
[SSOKie]: <http://ci-hiperion.sefin.gob.hn/>
[OpenKM]: <http://ci-hiperion.sefin.gob.hn/>
[Azure Board]: <http://ci-hiperion.sefin.gob.hn/>
[WIKI]: <http://ci-hiperion.sefin.gob.hn/>
[Gobernanza GIT]: <http://ci-hiperion.sefin.gob.hn/>
[Lineamientos de Base Datos]: <http://ci-hiperion.sefin.gob.hn/>

 


# SefinStyle

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.13.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

