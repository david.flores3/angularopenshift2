#FROM node:14-alpine as node
#
#USER root
#
#RUN mkdir -p /app
#
#WORKDIR /app
#
#COPY package.json /app
#
#RUN npm install
#
#COPY . /app
#
#RUN npm run build
#
## stage 2
##FROM nginx:1.17.1-alpine
#FROM nginxinc/nginx-unprivileged
#COPY --from=node /app/dist/vainilla-angular /usr/share/nginx/html



# Install the app dependencies in a full Node docker image
FROM registry.access.redhat.com/ubi8/nodejs-14:latest

# Copy package.json and package-lock.json
COPY package*.json ./

# Install app dependencies
RUN npm install --production

# Copy the dependencies into a Slim Node docker image
FROM registry.access.redhat.com/ubi8/nodejs-14-minimal:latest

# Install app dependencies
COPY --from=0 /opt/app-root/src/node_modules /opt/app-root/src/node_modules
COPY ./dist/vainilla-angular/* /opt/app-root/src

ENV NODE_ENV production
ENV PORT 3001

CMD ["npm", "start"]
